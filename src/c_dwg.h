/***************************************************************************

  c_dwg.h

  (C) 2020 Reini Urban <rurban@cpan.org>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2, or (at your option)
  any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
  MA 02110-1301, USA.

***************************************************************************/

#ifndef _C_DWG_H
#define _C_DWG_H

#include "gambas.h"
#include "main.h"

#include <stdint.h>

#ifndef _C_DWG_C

#define CLASSDESC(klass) extern GB_DESC klass##Desc[]
#define ENTITY_COLLECTION(token) CLASSDESC(token)

CLASSDESC (DwgDocument);
CLASSDESC (DxfDocument);
CLASSDESC (SummaryInfo);
CLASSDESC (Header);
ENTITY_COLLECTION (ModelSpace);
ENTITY_COLLECTION (PaperSpace);
ENTITY_COLLECTION (Blocks);
#undef ENTITY_COLLECTION

#define TABLE_COLLECTION(token,key) CLASSDESC(token)
TABLE_COLLECTION (DimStyles, DIMSTYLE);
TABLE_COLLECTION (Layers, LAYER);
TABLE_COLLECTION (Linetypes, LTYPE);
TABLE_COLLECTION (RegisteredApplications, APPID);
TABLE_COLLECTION (TextStyles, STYLE);
TABLE_COLLECTION (UCSs, UCS);
TABLE_COLLECTION (Viewports, VPORT);
TABLE_COLLECTION (Views, VIEW);
#undef TABLE_COLLECTION

#define DICT_COLLECTION(token,key) CLASSDESC(token)
DICT_COLLECTION (Dictionaries, NAMED_OBJECT_DICTIONARY);
DICT_COLLECTION (PlotConfigurations, PLOTSTYLES);
DICT_COLLECTION (Groups, GROUP);
DICT_COLLECTION (Colors, COLOR);
DICT_COLLECTION (LayoutsDesc, LAYOUT);
DICT_COLLECTION (MlineStylesDesc, MLINESTYLE);
DICT_COLLECTION (MLeaderStylesDesc, MLEADERSTLE);
DICT_COLLECTION (MaterialsDesc, MATERIAL);
DICT_COLLECTION (PlotStylesDesc, PERSUBENTMGR);
DICT_COLLECTION (DetailViewStylesDesc, DETAILVIEWSTYLE);
DICT_COLLECTION (SectionViewStylesDesc, SECTIONVIEWSTYLE);
DICT_COLLECTION (VisualStylesDesc, VISUALSTYLE);
DICT_COLLECTION (ScalesDesc, SCALELIST);
DICT_COLLECTION (TableStylesDesc, TABLESTYLE);
DICT_COLLECTION (WipeoutVarsDesc, WIPEOUT_VARS);
DICT_COLLECTION (AssocNetworksDesc, ASSOCNETWORK);
DICT_COLLECTION (AssocPersSubentManagersDesc, ASSOCPERSSUBENTMANAGER);
#undef DICT_COLLECTION

#define DWG_OBJECT(token) GB_DESC token##Desc[];
#define DWG_ENTITY(token) DWG_OBJECT(token)
#include "objects.inc"
#undef DWG_OBJECT 
#undef DWG_ENTITY 

#undef CLASSDESC
#else

#define THIS ((CDWGDOCUMENT *)_object)
#define THIS_DXF ((CDXFDOCUMENT *)_object)

#endif

#if LIBREDWG_VERSION_0_76

#define CDWG_list_get(_list, _i) ((_list)->at(_i))
#define CDWG_list_count(_list) ((_list)->size())

#else

#define CDWG_list_get(_list, _i) ((OutlineItem *)(_list)->get(_i))
#define CDWG_list_count(_list) ((_list)->getLength())

#endif

#define CDWG_index_get(_i) CDWG_list_get(THIS->index, _i)
#define CDWG_index_count() CDWG_list_count(THIS->index)

typedef struct {
  GB_BASE ob;
  //char *buf;
  //int len;
  Dwg_Data *data;
  bool is_dxf;
} CDWGDOCUMENT;
typedef CDWGDOCUMENT CDXFDOCUMENT;

#endif
